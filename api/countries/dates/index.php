<?php
// required headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
    header("HTTP/1.1 200 OK");
    die();
}

require_once("../../DatabaseCommunicator.php");


if($_SERVER["REQUEST_METHOD"] === "GET"){
    handleDatesGetRequest();
}
else if($_SERVER["REQUEST_METHOD"] === "POST"){
    handleDatesPostRequest();
}
else{
    http_response_code(405);
    echo json_encode(["ok" => false]);
}

function handleDatesPostRequest(){
    $json = file_get_contents('php://input');
    $data = json_decode($json);
    $name = $data->name;

    if(!$name || !isset($_GET["country"]) || !isset($_GET["date"])){
        http_response_code(404);
        echo json_encode(["ok" => false]);
        return;
    }

    $country = $_GET["country"];
    $date = $_GET["date"];

    $databaseCommunicator = new DatabaseCommunicator();

    if($databaseCommunicator->isNameSaved($name, $country)){
        http_response_code(400);
        echo json_encode(["ok" => false]);
        return;
    }

    $addNameInfo = ["name" => $name, "country" => $country, "date" => $date];
    $ok = $databaseCommunicator->addName($addNameInfo);

    if($ok){
        http_response_code(201);
        echo json_encode(["ok" => $ok]);
    }
    else {
        http_response_code(400);
        echo json_encode(["ok" => false]);
    }
}




function handleDatesGetRequest(){
    if(isset($_GET["country"]) && isset($_GET["type"]) && isset($_GET["date"])){
        handleDatesFullGetRequest();
    }
    else if(isset($_GET["country"]) && isset($_GET["date"])){
        handleDatesHalfGetRequest();
    }
    else if(isset($_GET["country"])){
        handleDatesSmallGetRequest();
    }
    else{
        http_response_code(405);
        echo json_encode(["ok" => false]);
    }

}

function handleDatesFullGetRequest(){
    $date = $_GET["date"];
    if(strlen($date) != 4 || !is_numeric($date)){
        http_response_code(404);
        echo json_encode(["ok" => false]);
        return;
    }

    $country = $_GET["country"];

    $type = $_GET["type"];
    if($type == "holidays"){
        $country .= "sviatky";
    }
    else if($type == "memorables"){
        $country .= "dni";
    }
    else if($type != "names"){
        http_response_code(501);
        echo json_encode(["ok" => false]);
        return;
    }

    $databaseCommunicator = new DatabaseCommunicator();
    $names = $databaseCommunicator->getNamesByDateAndIdType($date, $country);

    http_response_code(200);

    $jsonContent = ["ok" => true, "data" => [$type => $names]];
    $flags = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
    echo json_encode($jsonContent, $flags);
}

function handleDatesHalfGetRequest(){
    $date = $_GET["date"];
    if(strlen($date) != 4 || !is_numeric($date)){
        http_response_code(404);
        echo json_encode(["ok" => false]);
        return;
    }

    $country = $_GET["country"];

    $databaseCommunicator = new DatabaseCommunicator();
    $names = $databaseCommunicator->getNamesByDateAndIdType($date, $country);
    $holidays = $databaseCommunicator->getNamesByDateAndIdType($date, $country . "sviatky");
    $memorables = $databaseCommunicator->getNamesByDateAndIdType($date, $country . "dni");

    http_response_code(200);

    $jsonContent = ["ok" => true, "data" => ["names" => $names, "holidays" => $holidays, "memorables" => $memorables]];
    $flags = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
    echo json_encode($jsonContent, $flags);
}

function handleDatesSmallGetRequest(){
    $country = $_GET["country"];

    $databaseCommunicator = new DatabaseCommunicator();
    $allDateObjects = $databaseCommunicator->getAllDates();

    $allDates = [];
    foreach ($allDateObjects as $dateObject){

        $date = $databaseCommunicator->toUrlDate($dateObject);
        $names = $databaseCommunicator->getNamesByDateAndIdType($date, $country);
        $holidays = $databaseCommunicator->getNamesByDateAndIdType($date, $country . "sviatky");
        $memorables = $databaseCommunicator->getNamesByDateAndIdType($date, $country . "dni");

        $allDates[] = ["date" => $date, "content" => ["names" => $names, "holidays" => $holidays, "memorables" => $memorables]];

    }

    http_response_code(200);

    $jsonContent = ["ok" => true, "data" => ["dates" => $allDates]];
    $flags = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
    echo json_encode($jsonContent, $flags);
}

