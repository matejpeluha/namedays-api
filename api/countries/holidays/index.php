<?php
// required headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
    header("HTTP/1.1 200 OK");
    die();
}


require_once("../../DatabaseCommunicator.php");


if($_SERVER["REQUEST_METHOD"] === "GET"){
    handleHolidaysGetRequest();
}
else{
    http_response_code(405);
    echo json_encode(["ok" => false]);
}

function handleHolidaysGetRequest(){
    if(isset($_GET["country"]) && isset($_GET["holiday"]))
        handleHolidaysFullGetRequest();
    else if(isset($_GET["country"]))
        handleHolidaysSmallGetRequest();
    else{
        http_response_code(404);
        echo json_encode(["ok" => false]);
    }
}

function handleHolidaysFullGetRequest(){
    $country = $_GET["country"] . "sviatky";
    $holiday = $_GET["holiday"];

    $databaseCommunicator = new DatabaseCommunicator();
    $dateObject = $databaseCommunicator->getDateByNameAndCountry($holiday, $country);
    $date = $databaseCommunicator->toDate($dateObject);

    http_response_code(200);

    $jsonContent = ["ok" => true, "data" => ["date" => $date]];
    $flags = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
    echo json_encode($jsonContent, $flags);
}

function handleHolidaysSmallGetRequest(){
    $country = $_GET["country"] . "sviatky";

    $databaseCommunicator = new DatabaseCommunicator();
    $allHolidays = $databaseCommunicator->getNamesByCountry($country);

    http_response_code(200);

    $jsonContent = ["ok" => true, "data" => ["holidays" => $allHolidays]];
    $flags = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
    echo json_encode($jsonContent, $flags);
}
