<?php
// required headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
    header("HTTP/1.1 200 OK");
    die();
}

require_once("../DatabaseCommunicator.php");


if($_SERVER["REQUEST_METHOD"] === "GET"){
    handleCountriesGetRequest();
}
else{
    http_response_code(405);
    echo json_encode(["ok" => false]);
}

function handleCountriesGetRequest(){
    if(isset($_GET["country"]))
        handleCountriesFullGetRequest();
    else
        handleCountriesSmallGetRequest();

}


function handleCountriesFullGetRequest(){
    $country = $_GET["country"];

    $countryData = getCountryData($country);
    http_response_code(200);


    $jsonContent = ["ok" => true, "data" => $countryData];
    $flags = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
    echo json_encode($jsonContent, $flags);

}


function handleCountriesSmallGetRequest(){
    $databaseCommunicator = new DatabaseCommunicator();
    $allCountryObjects = $databaseCommunicator->getAllCountries();

    $data = [];
    foreach ($allCountryObjects as $countryObject){
        $data[] = ["country" => $countryObject["country"], "shortcut" => $countryObject["shortcut"] ];
    }

    $jsonContent = ["ok" => true, "data" => $data];
    $flags = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
    echo json_encode($jsonContent, $flags);
}


function getCountryData($country){
    $databaseCommunicator = new DatabaseCommunicator();
    $allDateObjects = $databaseCommunicator->getAllDates();

    $allNames = [];
    $allHolidays = [];
    $allMemorables = [];
    foreach ($allDateObjects as $dateObject){

        $date = $databaseCommunicator->toUrlDate($dateObject);
        $names = $databaseCommunicator->getNamesByDateAndIdType($date, $country);
        $allNames[] = ["date" => $date, "names" => $names];

        $holidays = $databaseCommunicator->getNamesByDateAndIdType($date, $country . "sviatky");
        $allHolidays[] = ["date" => $date, "holidays" => $holidays];

        $memorables = $databaseCommunicator->getNamesByDateAndIdType($date, $country . "dni");
        $allMemorables[] = ["date" => $date, "memorables" => $memorables];
    }

    return ["names" => $allNames, "holidays" => $allHolidays, "memorables" => $allMemorables];
}