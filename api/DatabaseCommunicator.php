<?php
require_once("config.php");

class DatabaseCommunicator
{
    private $connection;

    public function __construct()
    {
        $this->connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function isNameSaved($name, $country){
        $query = "SELECT records.name  FROM records WHERE records.name=:name AND records.id_type=:country";
        $bindParameters = [":name" => $name, ":country" => $country];

        $allNames = $this->getAssocResult($query, $bindParameters);

        foreach ($allNames as $savedName){
            if($savedName["name"] == $name)
                return true;
        }

        return false;

    }

    public function getAllDates(){
        $query = "SELECT dates.day, dates.month FROM dates ";
        $bindParameters = [];

        return $this->getAssocResult($query, $bindParameters);
    }

    public function getAllCountries(){
        $query = "SELECT types.shortcut, types.country FROM types WHERE type='Meno' ";
        $bindParameters = [];

        return $this->getAssocResult($query, $bindParameters);
    }

    public function getDateByNameAndCountry($name, $country){
        $query = "SELECT dates.month, dates.day FROM records  JOIN dates ON records.id_date = dates.id 
                    WHERE records.id_type=:country AND records.name=:name";
        $bindParameters = [":country" => $country, ":name" => $name];

        $allDates = $this->getAssocResult($query, $bindParameters);
        $dateObject = null;
        if(sizeof($allDates) > 0)
            $dateObject = $allDates[0];

        return $dateObject;
    }

    public function getNamesByDateAndIdType($date, $country)
    {
        $day = substr($date, 0, 2);
        $month = substr($date, 2, 2);

        $query = "SELECT records.name FROM records  JOIN dates ON records.id_date = dates.id 
                    WHERE records.id_type=:country AND dates.month=:month AND dates.day=:day";
        $bindParameters = [":country" => $country, ":month" => $month, ":day" => $day];

        $objectNames = $this->getAssocResult($query, $bindParameters);
        $names = [];
        foreach ($objectNames as $name){
            $names[] = $name["name"];
        }

        return $names;
    }

    public function getNamesByCountry($country){
        $query = "SELECT records.name, dates.month, dates.day FROM records JOIN dates ON records.id_date = dates.id 
                    WHERE records.id_type=:country";
        $bindParameters = [":country" => $country];
        $allNameObjects = $this->getAssocResult($query, $bindParameters);

        $resultNames = [];
        foreach ($allNameObjects as $nameObject){
            $dateObject = ["day" => $nameObject["day"], "month" => $nameObject["month"]];
            $date = $this->toDate($dateObject);
            $resultNames[] = ["name" => $nameObject["name"], "date" => $date];
        }

        return $resultNames;
    }


    public function addName($addNameInfo){
        $day = substr($addNameInfo["date"], 0, 2);
        $month = substr($addNameInfo["date"], 2, 2);
        $query = "SELECT dates.id FROM dates WHERE dates.day=:day AND dates.month=:month";
        $dateId = null;
        try {
            $bindParameters = [":day" => $day, ":month" => $month];
            $dateId = $this->getAssocResult($query, $bindParameters)[0]["id"];
        }catch (Exception $exception){
            return "fail";
        }



        if($dateId == null){
            return "fail";
        }


        $query = "INSERT INTO records (id_date, id_type, name) VALUES (?,?,?)";
        $bindParameters = [$dateId, $addNameInfo["country"], $addNameInfo["name"]];
        try {
          $statement = $this->connection->prepare($query);
            $statement->execute($bindParameters);
        }catch (Exception $exception){
            return "fail";
        }

        return "success";
    }



    private function getAssocResult($query, $bindParameters){
        $statement = $this->connection->prepare($query);
        $statement->execute($bindParameters);

        $statement->setFetchMode(PDO::FETCH_ASSOC);
        return $statement->fetchAll();
    }

    public function toDate($dateObject){
        if($dateObject == null)
            return null;

        $day = $dateObject["day"];
        if(strlen($day) < 2)
            $day = "0" . $day;

        $month = $dateObject["month"];
        if(strlen($month) < 2)
            $month = "0" . $month;

        $day .= ".";
        $month .= ".";

        return $day . $month;
    }

    public function toUrlDate($dateObject){
        if($dateObject == null)
            return null;

        $day = $dateObject["day"];
        if(strlen($day) < 2)
            $day = "0" . $day;

        $month = $dateObject["month"];
        if(strlen($month) < 2)
            $month = "0" . $month;

        return $day . $month;
    }




}