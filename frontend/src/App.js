import React, {Component} from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

import NameDayPage from "./components/NameDayPage";
import DocumentationPage from "./components/DocumentationPage";

import './App.css';


class App extends Component{
  render() {
      return (

          <Router basename={"/nameday-chill"}>
              <Switch>
                  <Route path={"/"} exact component={NameDayPage}/>
                  <Route path={"/api-documentation"} exact component={DocumentationPage}/>
              </Switch>
          </Router>

      );
  }
}

export default App;
