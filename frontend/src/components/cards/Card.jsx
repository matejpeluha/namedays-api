import React, {Component} from "react";
import "../../App.css";
import "../../css-components/documentation-page.css"
import "../../css-components/card.css"

class Card extends Component {
    render() {
        return (
            <div className={"request-container"}>
                <h5>{this.props.info.request}</h5>
                {this.getLink(this.props.info)}
                {this.getBody()}
            </div>
        );
    }

    getBody = () =>{
        if(this.props.info.request === "GET") {
            return (
                <React.Fragment>
                    <h5>Výstup</h5>
                    <div className={"json-container"}>
                        <pre>{JSON.stringify(this.props.info.output, null, 2)}</pre>
                    </div>
                </React.Fragment>
            );
        }
        if(this.props.info.request === "POST") {
            return (
                <React.Fragment>
                    <h5>Vstup</h5>
                    <div className={"json-container post-request-json-container"}>
                        <pre>{JSON.stringify(this.props.info.input, null, 2)}</pre>
                    </div>
                    <h5>Výstup</h5>
                    <div className={"json-container post-request-json-container"}>
                        <pre>{JSON.stringify(this.props.info.output, null, 2)}</pre>
                    </div>
                </React.Fragment>
            );
        }
    }

    getLink = (info) =>{
        return <a href={info.url} target="_blank" rel="noreferrer">{"..." + info.endpoints}</a>
    }
}

export default Card;