import React, {Component} from 'react';
import {Link} from "react-router-dom";
import MaterialTooltip from "@material-ui/core/Tooltip"

import "../css-components/name-day-page.css"
import NameForm from "./forms/NameForm";
import DateForm from "./forms/DateForm";
import HolidaysForm from "./forms/HolidaysForm";
import AddNameForm from "./forms/AddNameForm";


class NameDayPage extends Component {

    state = {
        activeButton: "name-button"
    }



    render() {
        return (
            <div id={"name-day-page-root"}>
                <h1>Nameday API </h1>
                <main>
                    <div className={"container"}>
                        <div id={"menu"}>
                            <button id={"name-button"} className={"sub-button sub-button-active"}
                                    onMouseLeave={this.handleOnMouseLeaveMenuButton}
                                    onClick={this.handleOnClickMenuButton}
                                    onMouseEnter={this.handleOnMouseEnterMenuButton}>
                                <span>Meno</span>
                            </button>
                            <button id={"date-button"} className={"sub-button "}
                                    onMouseLeave={this.handleOnMouseLeaveMenuButton}
                                    onClick={this.handleOnClickMenuButton}
                                    onMouseEnter={this.handleOnMouseEnterMenuButton}>
                                <span>Dátum</span>
                            </button>
                            <button id={"holiday-button"} className={"sub-button "}
                                    onMouseLeave={this.handleOnMouseLeaveMenuButton}
                                    onClick={this.handleOnClickMenuButton}
                                    onMouseEnter={this.handleOnMouseEnterMenuButton}>
                                <span>Sviatky</span>
                            </button>
                            <button id={"insert-button"} className={"sub-button "}
                                    onMouseLeave={this.handleOnMouseLeaveMenuButton}
                                    onClick={this.handleOnClickMenuButton}
                                    onMouseEnter={this.handleOnMouseEnterMenuButton}>
                                <span>Vložiť</span>
                            </button>
                        </div>
                        <NameForm activeButton={this.state.activeButton} />
                        <DateForm activeButton={this.state.activeButton} />
                        <HolidaysForm activeButton={this.state.activeButton} />
                        <AddNameForm activeButton={this.state.activeButton} />

                        <MaterialTooltip title={"Dokumentácia"}>
                            <div id={"doc-link"}>
                                <Link to={"/api-documentation"}>
                                    <i className="fas fa-book fa-2x" />
                                </Link>
                            </div>
                        </MaterialTooltip>


                    </div>
                </main>
                <footer>©Matej Peluha</footer>
            </div>
        );
    }

    handleOnClickMenuButton = (event) =>{
        document.getElementsByClassName("sub-button-active")[0].classList.remove("sub-button-active");
        event.currentTarget.classList.add("sub-button-active");

        this.setState({activeButton: event.currentTarget.id})
    }

    handleOnMouseEnterMenuButton = (event) =>{
        document.getElementsByClassName("sub-button-active")[0].classList.remove("sub-button-active");
        event.currentTarget.classList.add("sub-button-active");
    }


    handleOnMouseLeaveMenuButton = (event) =>{
        document.getElementsByClassName("sub-button-active")[0].classList.remove("sub-button-active");
        document.getElementById(this.state.activeButton).classList.add("sub-button-active");
    }

}

export default NameDayPage;