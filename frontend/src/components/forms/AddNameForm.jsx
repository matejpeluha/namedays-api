import React, {Component} from "react";

import "../../css-components/add-name-form.css";
import "../../css-components/name-day-page.css";
import  "../../App.css";
import CountriesSelect from "./form-inputs/CountriesSelect";
import DateInput from "./form-inputs/DateInput";


class AddNameForm extends Component {
    state = {
        message: ""
    }

    render() {
        return (
            <form id={"add-name-form"} onSubmit={this.handleOnSubmitForm} className={this.getVisibility()}>
                <div className={"input-container"}>
                    <label htmlFor={"name-input-addform"}>Zadajte meno</label>
                    <br/>
                    <input id={"name-input-addform"} type={"text"} placeholder={"meno"}/>
                </div>
                <DateInput inputId={"date-input-addform"}/>
                <CountriesSelect selectId={"countries-select-addform"}/>
                <h2>{this.state.message}</h2>
                <button className={"form-button"} type={"submit"}>Pridať meno</button>
            </form>
        );
    }

    getVisibility(){
        if(this.props.activeButton === "insert-button"){
            return "";
        }

        return "hide";
    }

    handleOnSubmitForm = (event) =>{
        event.preventDefault()
        let date =  document.getElementById("date-input-addform").value;
        date = date.split(".").join("");
        const country =  document.getElementById("countries-select-addform").value;
        const name =  document.getElementById("name-input-addform").value;

        const url = "https://wt118.fei.stuba.sk/nameday-chill/api/countries/" + country + "/dates/" + date;

        const inputObject = {name: name};
        const json = JSON.stringify(inputObject);

        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');


        let initObject = {
            method: 'POST', headers: reqHeader, body: json
        };

        const userRequest = new Request(url, initObject);

        fetch(userRequest)
            .then(this.getJsonFromResponse)
            .then(this.printResult)
            .catch(this.printError);
    }

    getJsonFromResponse = (response) =>{
        if(!response.ok){
            throw response;
        }
        return response.json();
    }

    printResult = (data) =>{
        const date =  document.getElementById("date-input-addform").value;
        const name =  document.getElementById("name-input-addform").value;
        const country =  document.getElementById("countries-select-addform").value;
         if( data.ok ){
             this.setState({message: name + " bol pridaný k meninám " + date + " v krajine " + country})
         }
         else{
             this.setState({message: name + " NEBOL pridaný k meninám " + date})
         }
    }

    printError = (err) =>{
        if(err.status === 400){
            this.setState({message: "NEPODARILO sa zapísať. Meno v tejto krajine už existuje."});
        }
        else {
            this.setState({message: "NASTALA CHYBA"});
        }
        console.log("Something went wrong!", err);
    }


}

export default AddNameForm;