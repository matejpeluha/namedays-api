import React, {Component} from "react";

import "../../css-components/name-form.css";
import "../../css-components/name-day-page.css"
import  "../../App.css"

import DateInput from "./form-inputs/DateInput";
import CountriesSelect from "./form-inputs/CountriesSelect";


class NameForm extends Component {

    state = {
        names: []
    }

    render() {
        return (
            <form  onSubmit={this.handleOnSubmitForm} className={this.getVisibility()}>
                <DateInput inputId={"date-input-nameform"}/>
                <CountriesSelect selectId={"countries-select-nameform"}/>
                <h2>{this.state.names.map(this.getNameSpan)}</h2>
                <button className={"form-button"} type={"submit"}>Vyhľadať meno</button>
            </form>
        );
    }

    getVisibility = () =>{
        if(this.props.activeButton === "name-button"){
            return "";
        }

        return "hide";
    }

    getNameSpan = (name, key) => {
        return <span key={key}>{name}</span>
    }

    handleOnSubmitForm = (event) =>{
        event.preventDefault();
        let date =  document.getElementById("date-input-nameform").value;
        date = date.split(".").join("");
        const country =  document.getElementById("countries-select-nameform").value;

        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'GET', headers: reqHeader
        };

        const url = 'https://wt118.fei.stuba.sk/nameday-chill/api/countries/' + country + "/dates/" + date + "/names";
        const userRequest = new Request(url, initObject);

        fetch(userRequest)
            .then(this.getJsonFromResponse)
            .then(this.saveResult)
            .catch(this.printError);
    }

    getJsonFromResponse = (response) =>{
        return response.json();
    }

    saveResult = (json) =>{
        let names = json.data.names;
        if(names.length < 1){
            names = ["Bez mena"];
        }
        this.setState({names: names});
    }

    printError = (err) =>{
        console.log("Something went wrong!", err);
    }
}

export default NameForm;
