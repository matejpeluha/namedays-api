import React, {Component} from "react";

import "../../../css-components/date-input.css"

class DateInput extends Component {
    render() {
        return (
            <div className={"input-container"}>
                <label htmlFor={"date-input"}>Zadajte dátum</label>
                <br/>
                <input id={this.props.inputId} placeholder={"dd.mm."} onChange={this.handleOnChangeDateInput} required/>
            </div>
        );
    }

    handleOnChangeDateInput = (event) =>{
        const input = event.currentTarget;
        const date = input.value;

        if(!date.includes(".")){
            input.setCustomValidity("Nevhodný formát dátumu [dd.mm.]");
            return;
        }

        const dateArray = date.split(".");
        dateArray.pop();
        if(dateArray.length !== 2){
            input.setCustomValidity("Nevhodný formát dátumu [dd.mm.]");
            return;
        }

        let day = dateArray[0];
        let month = dateArray[1];
        if(!this.isNumeric(day)){
            input.setCustomValidity("Nevhodný formát dátumu [dd.mm.]");
            return;
        }

        if(day.length === 1){
            day = "0" + day;
        }
        else if(day.length === 0){
            day = "01";
        }
        else if(day.length >=3){
            day = day.substr(0,2);
        }

        if(!this.isNumeric(month)){
            input.setCustomValidity("Nevhodný formát dátumu [dd.mm.]");
            return;
        }

        if(month.length === 1){
            month = "0" + month;
        }
        else if(month.length === 0){
            month = "01";
        }
        else if(month.length >=3){
            month = day.substr(0,2);
        }

        input.value = day + "." + month + ".";

        if(!this.isDateReal(day, month)){
            input.setCustomValidity("Neexistujúci dátum");
            return;
        }

        input.setCustomValidity("");

    }

    isNumeric(str) {
        return !isNaN(str) && !isNaN(parseFloat(str))
    }

    isDateReal(dayStr, monthStr){
        const day = parseInt(dayStr);
        const month = parseInt(monthStr);

        if(day < 1 || month < 1 || month > 12){
            return false;
        }
        else if(day > 29 && month === 2){
            return false;
        }
        else if(day > 30 && (month === 4 ||  month === 6 || month === 9 || month === 11)){
            return false
        }
        else if(day > 31 && (month === 1 || month === 3 || month === 5 || month === 7 || month === 8 || month === 10 || month === 12)){
            return false
        }

        return true;
    }
}

export default DateInput;