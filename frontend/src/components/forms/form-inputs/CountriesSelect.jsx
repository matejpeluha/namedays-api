import React, {Component} from "react";

class CountriesSelect extends Component {
    state = {
        countries: ["japonsko", "nemeckcco"]
    }

    constructor(props) {
        super(props);
        this.loadCountries()
    }

    render() {
        return (
            <div className={"input-container"}>
                <label htmlFor={"countries-select"}>Vyberte krajinu</label>
                <br/>
                <select id={this.props.selectId} name={"countries-select"} required>
                        {this.state.countries.map(this.getOptions)}
                </select>
            </div>
        );
    }

    getOptions = (countryInfo, key) =>{
       return <option key={key} value={countryInfo["shortcut"]}>{countryInfo["country"]}</option>;
    }

    loadCountries(){
        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'GET', headers: reqHeader
        };

        const userRequest = new Request('https://wt118.fei.stuba.sk/nameday-chill/api/countries/', initObject);

        fetch(userRequest)
            .then(this.getJsonFromResponse)
            .then(this.saveResult)
            .catch(this.printError);

    }

    getJsonFromResponse = (response) =>{
        return response.json();
    }

    saveResult = (data) =>{
        const allCountries = [];
        for(let countryInfo of data.data){
            allCountries.push(countryInfo);
        }

        this.setState({countries: allCountries});
    }

    printError = (err) =>{
        console.log("Something went wrong!", err);
    }

}


export default CountriesSelect;