import React, {Component} from "react";

import "../../App.css"
import "../../css-components/name-day-page.css";
import "../../css-components/holidays-form.css"

class HolidaysForm extends Component {
    state = {
        holidays:  []
    }


    render() {
        return (
            <form id={"holidays-form"}  onSubmit={this.handleOnSubmitForm} className={this.getVisibility()}>
                <div className={"input-container"}>
                    <label htmlFor={"holidays-select"}>Vyberte druh sviatku</label>
                    <br/>
                    <select id={"holidays-select"} name={"holidays-select"} required>
                        <option value={"SKsviatky"}>Slovenské sviatky</option>
                        <option value={"SKdni"}>Slovenské pamätné dni</option>
                        <option value={"CZsviatky"}>České sviatky</option>
                    </select>
                </div>
                <div id={"holidays-container"}>{this.state.holidays.map(this.getHolidayElement)}</div>
                <button className={"form-button"} type={"submit"}>Vyhľadať sviatky</button>
            </form>
        );
    }

    getVisibility = () =>{
        if(this.props.activeButton === "holiday-button"){
            return "";
        }

        return "hide";
    }

    getHolidayElement = (holiday, key) =>{
        return <p key={key}>{holiday.date + " " + holiday.name}</p>
    }

    handleOnSubmitForm = (event) =>{
        event.preventDefault();
        const holidayType =  document.getElementById("holidays-select").value;

        let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'GET', headers: reqHeader
        };

        const url = this.getUrl(holidayType);
        const userRequest = new Request(url, initObject);

        fetch(userRequest)
            .then(this.getJsonFromResponse)
            .then(this.saveResult)
            .catch(this.printError);
    }

    getUrl(holidayType){
        let url = 'https://wt118.fei.stuba.sk/nameday-chill/api/countries/';
        if(holidayType === "SKsviatky"){
            url += "SK/holidays/";
        }
        else if(holidayType === "SKdni"){
            url += "SK/memorables/";
        }
        else if(holidayType === "CZsviatky"){
            url += "CZ/holidays/";
        }
        else{
            url = "";
        }

        return url;
    }

    getJsonFromResponse = (response) =>{
        return response.json();
    }

    saveResult = (json) =>{
        let holidays = [];
        if(json.data.holidays){
            holidays = json.data.holidays;
        }
        else if(json.data.memorables){
            holidays = json.data.memorables;
        }
        this.setState({holidays: holidays});
    }

    printError = (err) =>{
        console.log("Something went wrong!", err);
    }
}

export default HolidaysForm;