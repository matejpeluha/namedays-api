import React, {Component} from "react";

import "../../css-components/date-form.css";
import "../../css-components/name-day-page.css";
import  "../../App.css";
import CountriesSelect from "./form-inputs/CountriesSelect";


class DateForm extends Component {
    state = {
        date: ""
    }

    render() {
        return (
            <form  onSubmit={this.handleOnSubmitForm} className={this.getVisibility()}>
                <div className={"input-container"}>
                    <label htmlFor={"name-input"}>Zadajte meno</label>
                    <br/>
                    <input id={"name-input-dateform"} type={"text"} placeholder={"meno"}/>
                </div>
                <CountriesSelect selectId={"countries-select-dateform"}/>
                <h2>{this.state.date}</h2>
                <button className={"form-button"} type={"submit"}>Vyhľadať dátum</button>
            </form>
        );
    }

    getVisibility = () =>{
        if(this.props.activeButton === "date-button"){
            return "";
        }

        return "hide";
    }

    handleOnSubmitForm = (event) =>{
        event.preventDefault();
        const name =  document.getElementById("name-input-dateform").value;
        const country =  document.getElementById("countries-select-dateform").value;

       let reqHeader = new Headers();
        reqHeader.append('Content-Type', 'application/json');

        let initObject = {
            method: 'GET', headers: reqHeader
        };

        const url = 'https://wt118.fei.stuba.sk/nameday-chill/api/countries/' + country + "/names/" + name;
        const userRequest = new Request(url, initObject);

        fetch(userRequest)
            .then(this.getJsonFromResponse)
            .then(this.saveResult)
            .catch(this.printError);
    }

    getJsonFromResponse = (response) =>{
        return response.json();
    }

    saveResult = (json) =>{
        let date = json.data.date;
        if(!date || date === ""){
            date = "Bez dátumu"
        }
        this.setState({date: date});
    }

    printError = (err) =>{
        console.log("Something went wrong!", err);
    }
}

export default DateForm;