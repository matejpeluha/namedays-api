import React, {Component} from "react";
import 'react-responsive-modal/styles.css';
import { Modal } from 'react-responsive-modal';

import "../App.css";
import "../css-components/documentation-page.css"
import Card from "./cards/Card";
import {Link} from "react-router-dom";


class DocumentationPage extends Component {

    state = {
        openModal: false,
        allInfo: [
            {
                request: "POST",
                url: "https://wt118.fei.stuba.sk/nameday-chill/api/countries/SK/dates/2412",
                endpoints: "/countries/{KRAJINA}/dates/{DÁTUM}",
                input: {
                    "name": "Miro Pele"
                },
                output: {
                    "ok": "success"
                }
            },
            {
                request: "GET",
                url: "https://wt118.fei.stuba.sk/nameday-chill/api/countries/",
                endpoints: "/countries/",
                output: {
                    "ok": true,
                    "data": [
                        {
                            "country": "Slovensko",
                            "shortcut": "SK"
                        },
                        {
                            "country": "Česko",
                            "shortcut": "CZ"
                        },
                        {
                            "country": "Poľsko",
                            "shortcut": "PL"
                        },
                        {
                            "country": "Maďarsko",
                            "shortcut": "HU"
                        },
                        {
                            "country": "Rakúsko",
                            "shortcut": "AT"
                        }
                    ]
                }
            },
            {
                request: "GET",
                url: "https://wt118.fei.stuba.sk/nameday-chill/api/countries/SK",
                endpoints: "/countries/{KRAJINA}",
                output: {
                    "ok": true,
                    "data": {
                        "names": [
                            {
                                "date": "dátum1",
                                "names": [
                                    "meno1",
                                    "meno2"
                                ]
                            },
                        ],
                        "holidays": [
                            {
                                "date": "dátum1",
                                "holidays": [
                                    "sviatok1"
                                ]
                            },
                        ],
                        "memorables": [
                            {
                                "date": "dátum1",
                                "memorables": [
                                    "pamätný_deň1"
                                ]
                            },
                        ]
                    }
                }
            },

            {
                request: "GET",
                url: "https://wt118.fei.stuba.sk/nameday-chill/api/countries/SK/names/",
                endpoints: "/countries/{KRAJINA}/names/",
                output: {
                    "ok": true,
                    "data": {
                        "names": [
                            {
                                "name": "meno1",
                                "date": "dátum1"
                            },
                            {
                                "name": "meno2",
                                "date": "dátum2"
                            },
                            {
                                "name": "meno3",
                                "date": "dátum3"
                            },
                        ]
                    }
                }
            },
            {
                request: "GET",
                url: "https://wt118.fei.stuba.sk/nameday-chill/api/countries/SK/names/Martina",
                endpoints: "/countries/{KRAJINA}/names/{MENO}",
                output: {
                    "ok": true,
                    "data": {
                        "date": "dátum"
                    }
                }
            },

            {
                request: "GET",
                url: "https://wt118.fei.stuba.sk/nameday-chill/api/countries/SK/holidays/",
                endpoints: "/countries/{KRAJINA}/holidays/",
                output: {
                    "ok": true,
                    "data": {
                        "holidays": [
                            {
                                "name": "sviatok1",
                                "date": "dátum1"
                            },
                            {
                                "name": "sviatok2",
                                "date": "dátum2"
                            },
                            {
                                "name": "sviatok3",
                                "date": "dátum3"
                            },
                        ]
                    }
                }
            },
            {
                request: "GET",
                url: "https://wt118.fei.stuba.sk/nameday-chill/api/countries/SK/holidays/Sviatok%20práce",
                endpoints: "/countries/{KRAJINA}/holidays/{NÁZOV SVIATKU}",
                output: {
                    "ok": true,
                    "data": {
                        "date": "dátum"
                    }
                }
            },

            {
                request: "GET",
                url: "https://wt118.fei.stuba.sk/nameday-chill/api/countries/SK/memorables/",
                endpoints: "/countries/{KRAJINA}/memorables/",
                output: {
                    "ok": true,
                    "data": {
                        "memorables": [
                            {
                                "name": "pamätný_deň1",
                                "date": "dátum1"
                            },
                            {
                                "name": "pamätný_deň2",
                                "date": "dátum2"
                            },
                            {
                                "name": "pamätný_deň3",
                                "date": "dátum3"
                            },
                        ]
                    }
                }
            },
            {
                request: "GET",
                url: "https://wt118.fei.stuba.sk/nameday-chill/api/countries/SK/memorables/Deň%20obetí%20Dukly",
                endpoints: "/countries/{KRAJINA}/memorables/{NÁZOV PAMÄTNÉHO DŇA}",
                output: {
                    "ok": true,
                    "data": {
                        "date": "dátum"
                    }
                }
            },

            {
                request: "GET",
                url: "https://wt118.fei.stuba.sk/nameday-chill/api/countries/SK/dates/",
                endpoints: "/countries/{KRAJINA}/dates/",
                output: {
                    "ok": true,
                    "data": {
                        "dates": [
                            {
                                "date": "dátum1",
                                "content": {
                                    "names": [
                                        "meno1"
                                    ],
                                    "holidays": [
                                        "sviatok1"
                                    ],
                                    "memorables": []
                                }
                            },
                            {
                                "date": "dátum2",
                                "content": {
                                    "names": [
                                        "meno1",
                                        "meno2",
                                        "meno3",
                                    ],
                                    "holidays": [],
                                    "memorables": [
                                        "pamätný_deň1"
                                    ]
                                }
                            },
                        ]
                    }
                }
            },
            {
                request: "GET",
                url: "https://wt118.fei.stuba.sk/nameday-chill/api/countries/SK/dates/2412",
                endpoints: "/countries/{KRAJINA}/dates/{DÁTUM}",
                output: {
                    "ok": true,
                    "data": {
                        "names": [
                            "meno1",
                            "meno2",
                            "meno3",
                        ],
                        "holidays": [
                            "sviatok1"
                        ],
                        "memorables": []
                    }
                }
            },
            {
                request: "GET",
                url: "https://wt118.fei.stuba.sk/nameday-chill/api/countries/SK/dates/2412/names",
                endpoints: "/countries/{KRAJINA}/dates/{DÁTUM}/names",
                output: {
                    "ok": true,
                    "data": {
                        "names": [
                            "meno1",
                            "meno2",
                            "meno3",
                        ],
                    }
                }
            },
            {
                request: "GET",
                url: "https://wt118.fei.stuba.sk/nameday-chill/api/countries/SK/dates/2412/holidays",
                endpoints: "/countries/{KRAJINA}/dates/{DÁTUM}/holidays",
                output: {
                    "ok": true,
                    "data": {
                        "holidays": [
                            "sviatok1"
                        ]
                    }
                }
            },
            {
                request: "GET",
                url: "https://wt118.fei.stuba.sk/nameday-chill/api/countries/SK/dates/0405/memorables",
                endpoints: "/countries/{KRAJINA}/dates/{DÁTUM}/memorables",
                output: {
                    "ok": true,
                    "data": {
                        "memorables": [
                            "pamätný_deň1"
                        ]
                    }
                }
            },
        ]
    }

    onOpenModal = () =>{
        this.setState({openModal: true, allInfo: this.state.allInfo})
    }

    onCloseModal = () =>{
        this.setState({openModal: false, allInfo: this.state.allInfo})
    }

    render() {
        return (
            <div id={"name-day-page-root"}>
                <div id={"icons-container"}>
                    <Link to={"/"}>
                        <i className="fas fa-arrow-circle-left fa-2x"/>
                    </Link>
                    <i onClick={this.onOpenModal} className="fas fa-info-circle fa-2x"/>
                    <Modal open={this.state.openModal} onClose={this.onCloseModal} center>
                        <div id={"modal-content"}>
                            <h3>
                                {"LINK"}
                            </h3>
                            <p>
                                <em>{"'...'"}</em>
                                {" pred endpointami značia "}
                                <a href={"https://wt118.fei.stuba.sk/nameday-chill/api"} target={"_blank"} rel="noreferrer">
                                    <em>{"https://wt118.fei.stuba.sk/nameday-chill/api"}</em>
                                </a>
                            </p>
                            <h3>
                                {"{DÁTUM}"}
                            </h3>
                            <p>
                                Zápise dňa(dd) a mesiaca(mm) vo formáte <em>ddmm</em> (napr. 0110, 2705, 2412)
                            </p>
                            <h3>
                                {"{KRAJINA}"}
                            </h3>
                            <table>
                                <tbody>
                                    <tr><td><em>SK</em></td><td>[Slovensko]</td></tr>
                                    <tr><td><em>CZ</em></td><td>[Česko]</td></tr>
                                    <tr><td><em>AT</em></td><td>[Rakúsko]</td></tr>
                                    <tr><td><em>HU</em></td><td>[Maďarsko]</td></tr>
                                    <tr><td><em>PL</em></td><td>[Poľsko]</td></tr>
                                </tbody>
                            </table>

                        </div>
                    </Modal>

                </div>
                <h1>Dokumentácia</h1>
                <main id={"documentation-container"}>
                    {this.state.allInfo.map(this.getCard)}
                </main>
                <footer>©Matej Peluha</footer>
            </div>
        );
    }

    getCard = (info, key) =>{
        return <Card key={key} info={info}/>;
    }
}

export default DocumentationPage;
